import React from 'react';
import { TabNavigator, StackNavigator, TabBarBottom } from 'react-navigation';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import Login from '../screens/Login';
import Settings from '../screens/Settings'
import Timeline from '../screens/Timeline';
import List from '../screens/List'
import Creation from '../screens/Creation'
import Maps from '../screens/Maps'
import Loading from '../screens/Loading'
import NotificationP from '../screens/Notification'

import Icon from 'react-native-vector-icons/FontAwesome';

const locationArrow = (<Icon name="location-arrow" size={21} color="white" />)
const map = (<Icon  name="th-list" size={21} color="white" />)
const bell = (<Icon name="bell" size={21} color="white" />)
const user = (<Icon name="user" size={21} color="white" />)

const accent = "#119D8F"

export const SetupStack = StackNavigator({
  List: {
    screen: List,
    navigationOptions: {
      title: 'Pets List',
    },
  },
},
{
  mode: 'modal',
  headerMode: 'none',
});

export const CreationStack = StackNavigator({
  Creation: {
    screen: Creation,
    navigationOptions: ({ navigation }) => ({
      title: "Profile Creation",
      headerStyle:{
        backgroundColor: accent,
      },
      headerTitleStyle:{
        color: 'white',
      }
    }),
  },
},
{
  mode: 'modal',
  headerMode: 'float',
});

export const Tabs = TabNavigator({
  Timeline: {
    screen: Timeline,
    navigationOptions: {
      tabBarLabel:locationArrow,
      tabBarIcon: ({ tintColor }) => <Icon name="location-arrow" size={35} color={tintColor} />,
    },
  },
  Maps: {
    screen: Maps,
    navigationOptions: {
      tabBarLabel: map,
      tabBarIcon: ({ tintColor }) => <Icon name="account-circle" size={35} color={tintColor} />
    },
  },
  Notification: {
    screen: NotificationP,
    navigationOptions: {
      tabBarLabel: bell,
      tabBarIcon: ({ tintColor }) => <Icon name="account-circle" size={35} color={tintColor} />
    },
  },
  User:{
    screen: Settings,
    navigationOptions: {
      tabBarLabel: user,
      tabBarIcon: ({ tintColor }) => <Icon name="account-circle" size={35} color={tintColor} />
    },
  }
},{
  tabBarPosition: 'bottom',
  TabBarBottom,
  tabBarOptions:{
    style:{
      backgroundColor: "black",
    },
  }
});

export const SettingsStack = StackNavigator({
  Settings: {
    screen: Settings,
    navigationOptions: {
      title: 'Settings',
    },
  },
});

export const Root = StackNavigator({

  Login:{
    screen: Login,
  },
  Setup: {
    screen: SetupStack,
  },
  Creation: {
    screen: CreationStack,
  },
  Loading:{
    screen: Loading,
  },
  Home:{
    screen: Tabs
  },
  Settings: {
    screen: SettingsStack,
  },
}, {
  mode: 'modal',
  headerMode: 'none',
});
