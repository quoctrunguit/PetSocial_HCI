import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    SectionList,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Alert
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import Post from './../components/post';
import Header from './../components/header';
import * as Animatable from 'react-native-animatable';
import Collapsible from 'react-native-collapsible'

const sendButton = (<Icon name="send" size={21} color="white" />)
const plus = (<Icon name="plus" size={21} color="white" />)

const inActiveColor  = 'black';
const baseSize= 60;

const pets=[{
    name: "puppy",
    breed: "Golden Retriever",
    age: 2,
    ageMeasurement: 'week',
    gender: 'male',
    sterilized: false,
    avatarSource: require('./../../assets/profiles/GR2.jpg'),
},{
    name: "Marcel",
    breed: "Capuchin",
    age: 3,
    ageMeasurement: 'month',
    gender: 'male',
    sterilized: false,
    avatarSource: require('./../../assets/profiles/capuchin.jpg'),
},{
    name: "Puth",
    breed: "Britain Pug",
    age: 1,
    ageMeasurement: 'year',
    gender: 'female',
    sterilized: false,
    avatarSource: require('./../../assets/profiles/eggsypug.jpg'),
},{
    name: "Lucky",
    breed: "Golden Retriever",
    age: 1,
    ageMeasurement: 'year',
    gender: 'female',
    sterilized: false,
    avatarSource: require('./../../assets/profiles/GR3.jpg'),
}]

const commentSamples = [
    {
        key: 1,
        data: {
            user: 'Jonathan',
           content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
           liked: false, 
        }
    },
    {
        key: 2,
        data: {
            user:'Monica Geller',
           content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
           liked: true, 
        }
    },
    {
        key: 3,
        data: {
            user:'Rachel Greene',
           content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
           liked: false, 
        }
    },
    {
        key: 4,
        data: {
            user: 'Chandler Bing',
           content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
           liked: false, 
        }
    }
]

const lessThan1km = [
    { 
        key: 5,
        data:{
            userName: "Benedict",
            postImage: require('./../../assets/PostImgs/GR1.jpg'),
            post: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
            numOfLike: 10,
            numOfComment: 5,
            pets:[{key:1 , data: pets[3]}],
            liked: false,
            comments: commentSamples,
        }
       },
    { 
    key: 6,
    data:{
        userName:"Jonnathan",
        postImage: require('./../../assets/PostImgs/playingwithRE.jpg'),
        post: "lorum ipsum bla bla",
        numOfLike: 10,
        numOfComment: 5,
        pets: [{key:1 , data: pets[0]}],
        liked: true,
        comments: commentSamples,
    }
    },
    { 
    key: 7,
    data:{
        userName: "Monica",
        postImage: null,
        post: "Anyone wants a monkey, because my brother has one and I don't like it.",
        numOfLike: 10,
        numOfComment: 5,
        pets:[],
        liked: false,
        comments: commentSamples,
    }
    },
];

const lessThan2km = [
    { 
        key: 8,
        data:{
            userName: "David Schwimmer",
            postImage:  require('./../../assets/PostImgs/rosswithmarcell.jpg'),
            post: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
            numOfLike: 10,
            numOfComment: 5,
            pets: [{key:1 , data: pets[1]}],
            liked: false,
            comments: commentSamples,
        }
       },
    { 
    key: 9,
    data:{
        userName:"Jonnathan",
        postImage: null,
        post: "Thanks for those who give me the dog last night. Hamilton was a family to me",
        numOfLike: 10,
        numOfComment: 5,
        pets: [{key:1 , data: pets[0]}],
        liked: false,
        comments: commentSamples,
    }
    },
    { 
    key: 10,
    data:{
        userName: "Richard",
        postImage: null,
        post: "Only dogs can heal any breakups",
        numOfLike: 10,
        numOfComment: 5,
        pets:[],
        liked: false,
        comments: commentSamples,
    }
    },
];

const lessThan3km = [
    { 
        key: 11,
        data:{
            userName: "Harry Hart",
            postImage:  require('./../../assets/PostImgs/harry_dog2.jpg'),
            post: "What is it like to lose your love ones?! 😿",
            numOfLike: 10,
            numOfComment: 5,
            pets: [{key:1 , data: pets[0]}],
            liked: false,
            comments: commentSamples,
        }
       },
    { 
    key: 12,
    data:{
        userName:"Jonnathan",
        postImage: null,
        post: "lorum ipsum bla bla",
        numOfLike: 10,
        numOfComment: 5,
        pets:[],
        liked: false,
        comments: commentSamples,
    }
    },
    { 
    key: 13,
    data:{
        userName: "Monica",
        postImage: null,
        post: "bla bla bla",
        numOfLike: 10,
        numOfComment: 5,
        pets:[],
        liked: false,
        comments: commentSamples,
    }
    },
];


const accentColor = '#009688';

const selectedItem = null;

const commentSection=(<View></View>)

export default class Timeline extends Component{

    constructor(props)
    {
        super(props);
        this.state={
            data1: lessThan1km,
            data2: lessThan2km,
            data3: lessThan3km,
            commentDisplay: false,
            selectedItem: null,
            headerText: 'Woofie!',
            commenting: false,
        }
    }

    render()
    {
        commentSection = this.getCommentSection(this.state.commentDisplay);

        return(
            <View style={{flex:1}}>
                <View style={styles.header}>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                        <View>
                            <Image style={styles.logo} source={require('./../../assets/logo-android.png')}/>
                        </View>
                        <View style={{position: 'absolute', right: 0, left: 0, top: 0, bottom: 0,  flexDirection: 'row', alignItems: 'center', justifyContent:'flex-end'}}>
                            <Icon style={{marginRight: 10}} size={20} name="map-o"/>
                        </View>
                    </View>
                    {/* <View style={{position: 'absolute', right: 0, height: 50, flexDirection: 'row', alignItems: 'center'}}>
                        <Icon.Button color='black' name="cog" backgroundColor='transparent'>
                        </Icon.Button>
                    </View> */}
                </View>
               
                <View style={{flex: 1}}>
                    <SectionList
                        style={styles.listBackground}
                        renderItem={({item}) => <Post key={item.key} data={item.data} itemSelected={this.commentClicked.bind(this)} />}
                        renderSectionHeader={({section}) => <Header title={section.title}/>}
<<<<<<< HEAD
                        
=======
                        renderSectionFooter={({section})=> <View style={{alignItems: 'center', marginTop: 5, marginBottom: 5}}><Text style={{color: accentColor}}>View all (+10)</Text></View>}
>>>>>>> b1b0d5ed583fbecf8bcb6bd7052fa6e1191936da
                        sections={[ // homogenous rendering between sections
                            {data:this.state.data1, title: "Less than 1km"},
                            {data: this.state.data2, title: "Less than 2km"},
                            {data: this.state.data3, title: "> 3km"},
                        ]}
                        stickySectionHeadersEnabled
                        //renderSectionFooter={({section}) => <Footer title={section.title}/>}
                    />
                    {commentSection}
                </View>

                <View style={{position: 'absolute', bottom: 20, right: 20}}>
                    <TouchableOpacity onPress={this.showPrompt.bind(this)}>
                        <View style={{width: 60, height: 60, borderRadius: 30, elevation: 5, marginLeft: 10, marginRight:10, marginTop: 10, marginBottom: 10, backgroundColor: accentColor, justifyContent:'center', alignItems: 'center'}}>
                            <Text>{plus}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
               
            </View>
        );
    }

    showPrompt()
    {
        Alert.alert('I\'m not ready yet', 'This is supposed to be feed editor...', [
            {text: 'OK'},
        ],)
    }

    commentClicked(item)
    {
        selectedItem = item;
        this.setState({commentDisplay: true});
    }

    getCommentSection(b)
    {
        console.log("displaying");

        if(!b || selectedItem == null)
        {
            return;
        }

        let item = selectedItem.comments;
        let comments = [];

        if(true)
        {
            for(var i = 0; i < item.length; i++)
            {
                let comment = item[i];
                let template  = (
                    <View key={i} style={{flexDirection: 'row', alignItems:'center'}}>
                        <View style={{flex: 0.2, alignItems: 'center', justifyContent: 'center'}}>
                            <View style={{backgroundColor: 'gray', width: baseSize, height: baseSize, borderRadius: baseSize/2}}/>
                        </View>
                        <View  style={styles.commentContainer}>
                            <Text style={styles.user}>{comment.data.user}</Text>
                            <Text style={styles.commentText}>{comment.data.content}</Text>
                            <View style={styles.bottomContainer}>
                                <Icon.Button color={comment.data.liked ? 'red': inActiveColor} 
                                            name={comment.data.liked?'heart': 'heart-o'} 
                                            backgroundColor='transparent' 
                                            onPress={()=>this.commentLikedButtonClicked()}>
                                    <Text style={{fontWeight: 'bold'}}>{comment.data.liked? 'Loved':''}</Text>
                                </Icon.Button>
                            </View>
                        </View>
                    </View>
                );
                comments.push(template);
            }
        }
      
        return(
            <Animatable.View ref="commentContainer" animation="slideInUp" 
                duration={300}
                onAnimationEnd={()=>{this.refs.commentTitle.props.style.opacity = 1; this.refs.commentTitle.slideInDown(200)}}
                style={styles.commentPopup}>
                <Animatable.View  style={{flex: 1}} duration={350} animation='slideInUp'>
                    <Animatable.View ref="commentTitle" style={{flex: 0.1, backgroundColor: 'white', opacity: 0}}>
                        <Text style={{ fontWeight: 'bold', fontSize: 20, marginLeft:20, marginTop: 10, color: 'black'}}>Comments ({selectedItem.numOfComment})</Text>
                    </Animatable.View>
                    <View style={{flex: 0.7}}>
                        <ScrollView >
                            {comments}
                        </ScrollView>
                    </View>

                    <View style={{flex:0.2, flexDirection: 'column', maxHeight: 200, elevation: 3, backgroundColor: 'white', justifyContent:'center', alignItems: 'stretch'}}>
                        <View>
                            <Text></Text>
                            <View style={{flexDirection: 'row', alignItems:'center', marginRight: 10}}>
                                <TextInput placeholder="your comment" onFocus={()=> this.setState({commenting: true})} onEndEditing={()=>this.setState({commenting: false})} style={[styles.commentInput]} underlineColorAndroid="transparent"></TextInput>
                                <TouchableOpacity >
                                    <View style={{alignItems: 'center', justifyContent: 'center', borderRadius:baseSize/2*0.618, height:baseSize*0.618, width: baseSize, backgroundColor:accentColor}}>
                                        <Text style={{color: 'white'}}>{sendButton}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{flexDirection: 'row', justifyContent:'center', borderWidth: 0}}>
                            <TouchableHighlight style={styles.normalButton} onPress={this.cancelButtonClicked.bind(this)}>
                                <View style={{flex:1, flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                                    <Icon size={20} name="close"/>
                                </View>
                            </TouchableHighlight>
                        </View>
                        
                    </View>
                </Animatable.View>
            </Animatable.View>
        )
    }

    cancelButtonClicked()
    {
       this.refs.commentContainer.fadeOutDown(300).then((endState)=>this.setState({commentDisplay: !this.state.commentDisplay}));
    }

    commentLikedButtonClicked()
    {

    }

    onActionSelected(position)
    {

    }

}

const styles = StyleSheet.create({
    list:{
        flex:1
    },
    listBackground:{
        backgroundColor: 'white'
    },
    header:{
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    headerText:{
        fontSize: 30,
        fontWeight: 'bold'
    },
    logo:{
        width:50,
        height:50
    },
    containerToolbar: {
        flexDirection: 'row',
        alignItems: 'stretch',
        backgroundColor: '#F5FCFF',
        height: 50,
        elevation: 3
      },
    navBarIconContainer:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    navBarIconContainerSelected:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: accentColor
    },
    normalButton:{
        backgroundColor: 'transparent',
        borderRadius: 20,
        width: 80,
        height: 40,
        marginRight: 20
    },
    blackText:{
        color:'black',
        fontWeight: 'bold',
        fontFamily: 'AwesomeFont'
    },
    commentText:{
        marginLeft: 5,
    },
    user:{
        fontSize: 15,
        fontWeight: 'bold',
        marginTop: 5,
        marginLeft: 5,
    },
    commentLiked:{

    },
    commentContainer:{
        flex: 0.8,
        marginTop: 5,
    },
    bottomContainer:{
    },
    commentBoxContainer: {
        flexDirection: 'row',

    },
    commentInput:
    {
        flex: 1,
        borderRadius: baseSize/2*0.618,
        
        borderWidth: 1,
        borderColor: 'gray',
        marginLeft: 5,
        marginRight: 5
    },
    commentPopup:{
        position: 'absolute',
        backgroundColor:'white',
        left: 5, right: 5, top: 5, bottom: 5, borderRadius: 10,
        elevation: 5,
        borderRadius: baseSize*0.382
    }
});

//AppRegistry.registerComponent('Timeline', ()=> Timeline);