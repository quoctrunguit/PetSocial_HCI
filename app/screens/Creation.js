import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
    Switch,
    Image,
    ScrollView,
    Dimensions,
    TouchableOpacity,
    
} from 'react-native';

import PetCreation from '../components/petCreation'
import TextSlides from '../components/textSlides'
import Icon from 'react-native-vector-icons/FontAwesome';
var ImagePicker = require('react-native-image-picker');

const {width, height} = Dimensions.get("window");
const plus = (<Icon name="plus" size={30} color="gray" />)
const addView =(<View style={[{backgroundColor: 'white', borderWidth: 2, borderColor:'gray', width: 80, height: 80, borderRadius: 40, alignItems: 'center', justifyContent: 'center'}]}>{plus}</View>)

export default class Creation extends React.PureComponent<{}>{

    constructor(props) {
        super(props);
        this.state={
            showGridlines: false,
            avatarSource: null,
            petInfo: null,
        }
    }

    componentDidMount()
    {

    }

    render()
    {
        var wireframe= this.state.showGridlines ? styles.wireframe : null;

        return(
           
            <ScrollView style={{flex: 1, backgroundColor: 'white'}}>
                 <View style={[{flex: 0.382, flexDirection: 'row'}, wireframe]}>
                    <View style={[{flex: 0.382}, wireframe]}>
                        <View style={[{flex: 0.6}, wireframe]}>
                        </View>
                        <View style={[{flex: 0.4, alignItems: 'center', justifyContent: 'flex-end'}, wireframe]}>
                            <TouchableOpacity onPress={this.changeProfile.bind(this)}>
                                {this.renderProfilePic()}
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={[{flex: 0.682}, wireframe]}>
                        <TextSlides/>
                    </View>
                </View>
                <View style={[{flex: 0.618}, wireframe]}>
                    <PetCreation doneClick={this.doneButtonClick.bind(this)} profileURI={this.state.avatarSource} cancelClick={this.cancelButtonClick.bind(this)}/>
                </View>
                <View  style={{position: 'absolute', right:0, bottom: 0}}>
                    <Switch value ={this.state.showGridlines} onValueChange={()=>this.setState({showGridlines: !this.state.showGridlines})}/>
                </View>
            </ScrollView>
        )
    }

    renderProfilePic()
    {
        var source=  this.state.avatarSource ?  <Image style={styles.profilePic} source={this.state.avatarSource}/>: addView;
        return source;
    }

    doneButtonClick(petInfo)
    {  
        this.props.screenProps.list.push({key: petInfo.name, data: petInfo});
        this.props.navigation.navigate("List");
    }

    cancelButtonClick()
    {
        this.props.navigation.navigate("List");
    }

    changeProfile()
    {
        var options = {
            title: 'Select a pet photo',
            storageOptions: {
              skipBackup: true,
              path: 'images'
            }
        };
          
 
        ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
            console.log('User cancelled image picker');
        }
        else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
        }
        else {
            let source = { uri: response.uri };

            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            console.log(this);
            this.setState({
            avatarSource: source
            });
        }
        });
    }

}

const styles = StyleSheet.create({
    wireframe: {
        borderWidth: 1
    },
    profilePic:{
        width: 80,
        height: 80,
        borderRadius: 50,
    },
})
