import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  ScrollView,
  CameraRoll,
  Easing,
  Animated,
  Alert,
  Dimensions,
  Switch
} from 'react-native';

import Collapsible from 'react-native-collapsible';

import PetCreation from './../components/petCreation';
import PetAddition from '../components/petAddition'
import PetList from './../components/petList';
import SetupStack from './../components/setupStack';
import * as Animatable from 'react-native-animatable'

import Menu, { MenuContext, MenuOptions, MenuOption, MenuTrigger } from 'react-native-menu';

const {width, height} = Dimensions.get("window");

export default class List extends Component{

    constructor(props) {
        super(props);
        this.state={
            list: this.props.screenProps.list,
            showGridlines: false
        }
    }

    componentDidMount()
    {
        
    }

    render(){
        var wireframe = this.state.showGridlines ? styles.wireframe : null;
        return(
            <View style={{flex: 1}}>
                <View style={[{flex:0.382,flexDirection: 'row'}, wireframe]}>
                    <View style={{flex: 0.382}}>
                    </View>
                    <View style={[{flex: 0.618, alignItems: 'flex-end'}, wireframe]}>
                        <Animatable.Image animation='slideInDown' direction="alternate" style={styles.dogPaw} source={require('./../../assets/Dog-Paws.png')}/>
                    </View>
                </View>
                <View style={[{flex: 0.618}, wireframe]}>
                   {/* <SetupStack navigation= {this.props.navigation}/> */}
                   {this.renderList()}
                    <View  style={{position: 'absolute', right:0, bottom: 0}}>
                        <Switch value ={this.state.showGridlines} onValueChange={()=>this.setState({showGridlines: !this.state.showGridlines})}/>
                    </View>
                </View>
            </View>
        );
    }

    renderList()
    {
        if(!this.state.list)
            return null;
        var display = this.state.list.length > 0 ? <PetList list={this.state.list} 
                                                            addClick={this.addButtonClick.bind(this)}
                                                            finishClick={this.finishButtonClick.bind(this)}
                                                            cancelClick ={this.cancelButtonClick.bind(this)}
                                                            showGridlines = {this.state.showGridlines}
                                                            editFunc={this.editAPet.bind(this)}/> :  
                                                            <PetAddition addClick={this.addButtonClick.bind(this)} createClick={this.createAPet.bind(this)}
                                                                    showGridlines = {this.state.showGridlines}/>
        return(
           display
        )
    }

    addButtonClick()
    {
        this.props.navigation.navigate("Creation");
    }
    createAPet(petInfo)
    {

    }

    editAPet(data)
    {
        this.props.navigation.navigate("Creation", {editItem: data});
    }

    finishButtonClick()
    {
        this.props.navigation.navigate("Loading")
    }

    cancelButtonClick()
    {
        Alert.alert('Discard This Profile', 'Are you sure', [
            {text: 'Yes',  onPress: ()=>  this._clearProfile()},
            {text: 'Cancel', }
        ],)
    }

    _clearProfile()
    {
        this.props.screenProps.list = []
        this.props.navigation.navigate("Login")
    }
}


const styles = StyleSheet.create(
    {
        dogPaw:{
            height:180,
            width: 210,
            transform: [{rotateZ:'180deg'}]
        },
        welcome:{
            flex:1,
            flexDirection: 'column',
            alignItems: 'center',
            marginTop: 150
        },
        header:{
            fontSize: 30,
            marginLeft: 20,
            color: 'black'
        },
        headerBold:{
            fontWeight: 'bold'
        },
        subHeader:{
            fontSize: 20,
            marginLeft:20
        },
        plusSymbol:{
            flex:1,
            fontSize: 50,
            color: 'white',
            textAlign: 'center',
            textAlignVertical: 'center',
        },
        addButton:{
            width: 100,
            height: 100,
            marginTop: 50,
            backgroundColor: 'black',
            backgroundColor: '#009688',
            borderRadius: 50,
            alignItems: 'center'
        },
        wireframe: {
            borderWidth: 1
        }
    }
)


//AppRegistry.registerComponent('Welcome',() => Welcome);