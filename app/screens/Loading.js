import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TextInput,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome'

const accentColor = '#009688';


const backgroundMap = require('../../assets/map_background.png');

const {width, height} = Dimensions.get("window");

const panelHeight = height*0.382*0.618;
const iconSize = panelHeight*0.382

const locationPin = (<Icon name="map-marker" size={iconSize} color={accentColor} />)

export default class Loading extends React.PureComponent<{}>{

    constructor(props)
    {
        super(props)
        this.state={
            loadingIndicator: '',
        }
    }

    componentDidMount()
    {
        var timer = setInterval(() => {
            if(this.state.loadingIndicator.length > 3)
                this.setState({loadingIndicator: ''});
            var indicator = this.state.loadingIndicator + '.';
            this.setState({loadingIndicator: indicator});
        }, 500);

        setTimeout(()=>{
            this.props.navigation.navigate("Timeline");
            clearInterval(timer);
        }, 4000)
    }

    render()
    {
        return(
            <View style={{flex: 1, justifyContent: 'center'}}>
                <Image style={[{position: 'absolute', ...StyleSheet.absoluteFillObject}]} source={backgroundMap}/>
                <View style={{ backgroundColor: 'white', height: panelHeight, alignItems:'center',justifyContent:"center"}}>
                    <Text style={{marginTop: 0}}>{locationPin}</Text>
                    <Text style={{marginTop: 10, textAlign:'center'}}>Fetching
                    <Text style={{color: accentColor}}>  Pet Feeds </Text>
                    nearby{"\n"}{this.state.loadingIndicator}</Text>
                </View>
            </View>
        )
    }
}


