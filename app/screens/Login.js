import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
  Switch,
} from 'react-native';

import SignIn from './../components/sign-in'

export default class Login extends Component{

    constructor(props) {
        super(props);
         this.state = { 
             textPasswordBox: null,
             textUsernameBox: null,
             showGridlines: false,
         }
    }

    render(){

        var border = this.state.showGridlines ? styles.bordered : null;

        return <View style={styles.container}>
            <View style={{ flex: 0.1 }} />
            <View style={{ flex: 0.7, marginLeft: 40, marginRight: 40 }}>
              <View style={[styles.upperContainer, border]}>
                <View style={styles.logoContainer}>
                  <Image style={{ width: 60, height: 60 }} source={require("../../assets/logo-android.png")} />
                  <Text style={{ fontSize: 30 }}>Woofie</Text>
                </View>

<<<<<<< HEAD
                <View style={[styles.inputContainer]}>
                  <TextInput style={styles.TextInput} placeholder="User name" onChangeText={textUsernameBox => this.setState(
                        { textUsernameBox }
                      )} value={this.state.textUsernameBox} />
                  <TextInput style={styles.TextInput} placeholder="Password" onChangeText={textPasswordBox => this.setState(
                        { textPasswordBox }
                      )} value={this.state.textPasswordBox} />
=======
                    <View style={[styles.inputContainer]}>
                        <TextInput underlineColorAndroid="transparent" style={styles.TextInput} placeholder="User name" onChangeText={textUsernameBox => this.setState(
                            { textUsernameBox }
                            )} value={this.state.textUsernameBox} />
                        <TextInput underlineColorAndroid="transparent" style={styles.TextInput} placeholder="Password" onChangeText={textPasswordBox => this.setState(
                            { textPasswordBox }
                            )} value={this.state.textPasswordBox} />
                    </View>
>>>>>>> b1b0d5ed583fbecf8bcb6bd7052fa6e1191936da
                </View>
              </View>

              <View style={[styles.lowerContainer, border]}>
                <View style={[styles.LoginContainer, border]}>
                  <View style={[styles.container4, , border]}>
                    <TouchableOpacity placeholder={"Login"} onPress={this.onClick.bind(this)} style={styles.button}>
                      <Text style={styles.text}>Login</Text>
                    </TouchableOpacity>
                    <View style={[styles.container5]} />
                  </View>
                </View>

                <View style={[styles.LoginWithFB]}>
                  <Text style={[styles.txtFBLogo]}> f </Text>
                  <Text style={[styles.txtFBLogin]}>
                    Login with Facebook
                  </Text>
                </View>
<<<<<<< HEAD

                <View style={[styles.ForGetPass]}>
                  <Text>Forget your Password?</Text>
                </View>
              </View>
            </View>
            <View style={{ flex: 0.2 }}>
              <Switch value={this.state.showGridlines} onValueChange={() => this.setState(
                    { showGridlines: !this.state.showGridlines }
                  )} />
=======
            </View>   
            <View style={{flex: 0.2}}>
                <Switch value ={this.state.showGridlines} onValueChange={()=>this.setState({showGridlines: !this.state.showGridlines})}/>
                <Switch value ={this.state.skipLogin} onValueChange={()=>this.setState({skipLogin: !this.state.skipLogin})}/>
>>>>>>> b1b0d5ed583fbecf8bcb6bd7052fa6e1191936da
            </View>
          </View>;
    }

    onClick(){
      if(this.state.skipLogin)
      {
        this.props.navigation.navigate("Timeline");
      }else{
        this.props.navigation.navigate('List');
      }
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    alignContent: "center",
    backgroundColor: "white"
  },
  lowerContainer: {
    flex: 0.382,
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    justifyContent: "flex-end"
  },
  LoginContainer: {
    flex: 0.382,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignContent: "center"
  },
  container4: {
    flex: 0.382,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignContent: "center"
  },
  container5: {
    // flex: 0.382,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignContent: "center"
  },
  button: {
    backgroundColor: "#009688",
    borderRadius: 21,
    width: 100,
    height: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  signinContainer: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    marginTop: 150
  },
  text: {
    color: "white",
    fontWeight: "bold"
  },
  inputContainer: {
    flex: 0.382,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-end"
  },
  upperContainer: {
    flex: 0.618,
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "white"
  },
  TextInput: {
    marginTop: 10,
    height: 60 * 0.618,
    width: 280,
    borderRadius: 21,
    borderWidth: 1,
    borderColor: "gray",
    fontSize: 13
  },
  logoContainer: {
    flex: 0.618,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  bordered: {
    borderWidth: 1
  },
  LoginWithFB: {
    position: "absolute",
    bottom: 10,
    left: 0,
    right: 0,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  ForGetPass: {
    position: "absolute",
    bottom: 110,
    left: 0,
    right: 100,
    justifyContent: "center",
    alignItems: "center"
  },
  txtFBLogin: {
    fontSize: 18,
    //fontWeight: "bold",
    color: "#2699FB"
  },
  txtFBLogo: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#2699FB"
  }
});


AppRegistry.registerComponent('HomePage',() => HomePage);