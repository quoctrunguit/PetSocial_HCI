import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
} from 'react-native';

export default class Header extends Component{

    constructor(props)
    {
        super(props);
        this.state={
            title:this.props.title
        }
    }

    render()
    {
        return(
            <View>
                <View style={styles.headerContainter}>
                    <Text style={styles.header}>{this.state.title}</Text>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    headerContainter:{
        backgroundColor: 'white',
        height: 50,
        marginBottom: 5,
        elevation: 2
    },
    header:{
        marginLeft: 20,
        marginTop: 10,
        fontSize: 20
    }
});

AppRegistry.registerComponent('Header', ()=> Header);