import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    findNodeHandle
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import * as Animatable from 'react-native-animatable';

const accentColor = 'black'

const inActiveColor  = 'black';
const activeColor = 'red';

const pics = [];

var slideUp = null;
var slideDown = null;

var initialLayout = null;

let s = false;

export default class Post extends Component{

    constructor(props)
    {
        super(props);
        this.state={
            userName: this.props.data.userName,
            postImage: this.props.data.postImage,
            post: this.props.data.post,
            numOfLike: this.props.data.numOfLike,
            numOfComment: this.props.data.numOfComment,
            pets: this.props.data.pets,
            liked: this.props.data.liked,
            viewSize: {x: 0, y: 0, width: 0, height: 0},
            animation: null,
            displayPetInfo: false,
            selectedPet:'',
            height: 'auto'
        }
    }

    measureView(event)
    {
        let size= {
            x: event.nativeEvent.layout.x,
            y: event.nativeEvent.layout.y,
            width: event.nativeEvent.layout.width,
            height: event.nativeEvent.layout.height
        };
    }

    setInitialLayout(event)
    {
        if(!s)
        {
            initialLayout = event.nativeEvent.layout;
          
        }
    }

    // initializeAnimation()
    // {
    //     slideUp = {
    //         0:{
    //             top: this.state.viewSize.height -60
    //         },
    //         0.5:{
    //             top: this.state.viewSize.height/2
    //         },
    //         1:{
    //             top :0
    //         }
    //     }
    //     slideDown = {
    //         to:{
    //             top: this.state.viewSize.height - 60
    //         }
    //     }
    // }


    render()
    {
        return(
            <View onLayout={(event)=> this.setInitialLayout(event)} style={{height: this.state.height}} ref="container">
                <View style={styles.container}>
                    <View>
                    </View>
                    <View onLayout={(event)=> this.measureView(event)} style={[styles.canvasContainer, {height: this.state.postImage? 200: 0}]}>
                        <Image
                        style={styles.canvas} 
                        resizeMode='contain' 
                        source={this.state.postImage}/>
                    </View>
                    <View style={{backgroundColor:'white', marginBottom: 60}}>
                        <Text style={styles.name}>{this.state.userName}</Text>
                        <Text style={styles.post}>{this.state.post}</Text>
                    </View>
                    
                </View>
                <View style={styles.petList}>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', borderWidth: 0}}> 
                            <View style={styles.likeCommentContainer}>
                                <Animatable.View ref="likeButton">
                                    <Icon.Button color={this.state.liked ? 'red': inActiveColor} 
                                                name={this.state.liked?'heart': 'heart-o'} 
                                                backgroundColor='transparent' 
                                                onPress={()=>this.likeButtonClicked()}>
                                        <Text>{this.state.numOfLike}</Text>
                                    </Icon.Button>
                                </Animatable.View>
                                <Icon.Button color='black' 
                                            name="comment-o" 
                                            backgroundColor='transparent'
                                            onPress={()=>this.commentButtonClicked()}>
                                    <Text>{this.state.numOfComment}</Text>
                                </Icon.Button>
                            </View>
                        </View>
                    {this.displayPetList()}
                    {this.displayPetInfo(this.state.pets)}
                </View>
            </View>
        );
    }

    likeButtonClicked()
    {
        this.refs.likeButton.bounceIn(800);
        this.setState({liked: !this.state.liked}, ()=> this.setState({numOfLike: this.state.liked? this.state.numOfLike +1: this.state.numOfLike -1}) );
    }

    commentButtonClicked()
    {
        this.props.itemSelected(this.props.data)
    }

    displayPetList()
    {

        let petLogos = [];
        if(this.state.pets==null)
            return;

        for(var i = 0; i< this.state.pets.length; i++)
        {
            let item = this.state.pets[i];
            let refname= i + "view";
            let template = (
                <Animatable.View ref={refname} key={i} style={{elevation: 2}}>
                    <TouchableHighlight onPress={()=> this.petIconPress(refname)}>
                        <Image style={styles.petProfile} source={item.data.avatarSource}/>
                    </TouchableHighlight>
                </Animatable.View>
            );
            petLogos.push(template);
        }
        return petLogos;
    }

    displayPetInfo(info)
    {
        if(this.state.displayPetInfo == false)
            return; 

        console.log("thong tin", info[0].data.avatarSource);
        let petinfo =  info[0].data;

        return(
            <Animatable.View animation="slideInRight" duration={300} ref="petProfileContainer" style={styles.petInfoContainer} 
            onAnimationEnd={()=>{   this.refs.petProfile.props.style.opacity = 1;
                                    this.refs.petProfile.zoomIn(200)
                                }}>
                <Animatable.View style={{borderWidth: 2, alignItems: 'center', opacity: 0}} ref="petProfile">
                    <Image style={[styles.petProfile, {width: 100, height: 100, marginTop: 20}]} source={petinfo.avatarSource}/>
                </Animatable.View>
                <View style={{flex: 1, flexDirection: 'column', marginLeft: 20, marginRight: 20}}>
                    <View style={{flex: 1, alignItems: 'center'}}>
                        <Text style={[styles.petInfoText, {marginTop: 0, fontSize: 30, fontWeight: 'bold'}]}>{petinfo.name}</Text>
                    </View>
                    <View style={{flex: 3, alignItems: 'flex-start'}}>
                        <Text style={[styles.petInfoText, {fontSize: 20}]}>{petinfo.breed}</Text>
                        <Text style={[styles.petInfoText, {fontSize: 15}]}>Age: {petinfo.age} {petinfo.ageMeasurement}{petinfo.age>0?'s':''} old</Text>
                        <Text style={[styles.petInfoText, {fontSize: 15}]}>{petinfo.gender}</Text>
                        <Text style={[styles.petInfoText, {fontSize: 15}]}>{petinfo.sterilized ? 'sterilized':''}</Text>
                    </View>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{flex: 1}}>
                            <Icon.Button name="arrow-right"  color='white' backgroundColor='transparent'>
                                <Text  style={[styles.petInfoText, {marginTop: 0, fontSize: 15}]}>see {petinfo.name}'s profile</Text>
                            </Icon.Button>
                        </View>
                        <View style={{flex: 1, alignItems: 'flex-end'}}>
                            <Animatable.View ref="closeButton">
                                <TouchableHighlight  style={styles.normalButton} onPress={()=>this.cancelButtonClicked()}>
                                    <Icon size={25} color='white' name="close"/>
                                </TouchableHighlight>
                            </Animatable.View>
                        </View>
                        
                    </View>
                </View>
            </Animatable.View>
        )
        //return (<View/>)
    }

    petIconPress(ref)
    {  
        let object = this.refs[ref];
        this.setState({selectedPet: ref})
        object.zoomOut(100).then((endState)=> this.refs.closeButton.zoomIn(200))
        this.showPetProfile();
    }

    cancelButtonClicked(){
        this.setState({height: 'auto'});
        this.refs[this.state.selectedPet].zoomIn(300);
        this.showPetProfile();
    }

    showPetProfile()
    {
        console.log("Pressed");
        if(this.state.displayPetInfo)
        {
            
           this.refs.petProfileContainer
           .fadeOutRight(200)
           .then((endState)=> this.setState({displayPetInfo: !this.state.displayPetInfo}));
        }else{
            if(initialLayout.height < 200)
            {
                this.setState({height: 300});
            }

            this.setState({displayPetInfo: !this.state.displayPetInfo});
        }
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 5,
        backgroundColor: 'white',
    },
    likeCommentContainer: {
        marginTop: 20,
        marginLeft: 10,
        flexDirection: 'row',
    },
    post:{
        marginLeft: 10,
        marginRight: 10
    },
    name:{
        marginLeft: 10,
        fontWeight: 'bold',
        fontSize: 20
    },
    heart:{
        color: 'red'
    },
    comment:{
        color: accentColor
    },
    canvas: {
        position: 'absolute',
        width: 400,

    },
    canvasContainer: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
        position: 'relative'
    },
    petInfoContainer:{
            position: 'absolute',
            top: 0,
            left: 0,
            bottom:0,
            right: 0, 
            flex: 1,
            backgroundColor: 'rgba(0, 0, 0, 0.9)',
            borderWidth:  1
        },
    petProfile:{
        width: 50,
        height: 50,
        borderRadius: 50,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5
    },
    petList:{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        flexDirection:'row', 
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        borderWidth: 0,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 5,
        elevation: 2
    },
    petsBanner:{
        position: 'absolute',
        flexDirection:'row', 
        justifyContent:'flex-end', 
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
    },
    absolute: {
        position: "absolute",
        top: 0, left: 0, bottom: 0, right: 0,
      },

    petInfoText:{
        color: 'white',
        flexDirection: 'row',
        borderColor:'white',
        borderWidth: 0,
        marginTop: 5
    }
});

AppRegistry.registerComponent('Post', ()=> Post);