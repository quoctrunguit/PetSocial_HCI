import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  TextInput,
  Picker,
  Slider,
  Button,
  TouchableOpacity,
  Switch,
  ScrollView,
  Alert
} from 'react-native';

import Collapsible from 'react-native-collapsible';
import Icon from 'react-native-vector-icons/FontAwesome';
import {RadioButtons} from 'react-native-radio-buttons';
import {SegmentedControls} from 'react-native-radio-buttons';
import * as Animatable from 'react-native-animatable'

var ImagePicker = require('react-native-image-picker');

var radio_props = [
    {label: 'param1', value: 0 },
    {label: 'param2', value: 1 }
];

const backBtn = (
    <Icon.Button name="chevron-left" color='black' style={{marginLeft:20}} backgroundColor='transparent' onPress={this.backButtonClick}></Icon.Button>
    );

const questionCircle = (<Icon name="question-circle" size={30} color="gray" />)


const genderOptions = [
    {
        label: 'male',
        value: 'm'
    },
    {
        label: 'female',
        value: 'f'
    },
    {
        label: 'none',
        value: 'n'
    }
    ]


const ageMesurementOptions = [
    {
      label: 'week',
      value: 'w'
    },
    {
      label: 'month',
      value: 'm'
    },
    {
        label: 'year',
        value: 'y'
    }
  ]

const accentColor = '#009688';

var options = {
    title: 'Select Avatar',
    customButtons: [
      {name: 'fb', title: 'Choose Photo from Facebook'},
    ],
    storageOptions: {
      skipBackup: true,
      path: 'images'
    }
  };

var initialState;

var nameInput= breedInput= ageInput= genderInput =false;

export default class PetCreation extends React.PureComponent{

    constructor(props)
    {
        super(props);
        this.state = {
            key: guid(),
            name: "",
            breed: "",
            age: 0,
            ageMeasurement: 'week',
            gender: 'none',
            sterilized: false,
            avatarSource: null,
            maxAgeValue: 1,
            visibility: false,
            ageMeasurementSelected: false,
            update: false,
            displayControls: false,
            inputState: false,
            namePrompt: false,
            breedPrompt: false,
        }
        this.initialState = this.state;
    }

    renderOption(option, selected, onSelect, index){
        const style = selected ? { color: 'blue'} : {};
     
        return (
          <TouchableWithoutFeedback onPress={onSelect} key={index}>
            <Text style={style}>{option}</Text>
          </TouchableWithoutFeedback>
        );
    }
    
    renderContainer(optionNodes){
        return <View>{optionNodes}</View>;
    }
    
    render(){
        var visibility = nameInput && breedInput;
        return(
            <View>
                 <View style={{}}>
                    <View style={styles.field}>
                        {/* <Text style={styles.fieldTitle}>Name</Text> */}
                        {this.namePrompt()}
                        <TextInput ref="nameInput" placeholder="Your pet's name" returnKeyType='next' onEndEditing={this.nameEditedEnd.bind(this)}  
                            style={styles.textInput} value={this.state.name} 
                            onChangeText={(value)=> this.nameInput(value)}
                            ></TextInput>
                        {/* <View style={{borderWidth: 1, opacity: 0.5, marginRight: 20}}/> */}
                    </View>

                    {/*Breed Section*/}
                    <View style={styles.field}>
                        {this.breedPrompt()}
                        <TextInput  ref="breedInput" placeholder="Breed" returnKeyType='done' style={styles.subTextInput} value={this.state.breed} onEndEditing={this.breedEditingEnd.bind(this)} onChangeText={(value)=> this.breedInput(value)}></TextInput>
                        {/* <View style={{borderWidth: 1, opacity: 0.5, marginRight: 20}}/> */}
                    </View>

                    {/*Age Section*/}
                    <Collapsible collapsed={!visibility} style={styles.field}>
                        <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                            <View style={{flex:1, marginTop: 20}}>
                                <Text style={styles.fieldTitle}>Age</Text>
                            </View>
                            <View style={styles.dialog}>
                                <View style={{width: 150, justifyContent: 'flex-start'}}>
                                    <SegmentedControls
                                        //optionStyle={{borderWidth: 2}}
                                        optionContainerStyle={styles.segmentControl}
                                        tint={'#009688'}
                                        options={ ageMesurementOptions }
                                        containerBorderWidth={0}
                                        extractText={ (option) => option.label }
                                        selectedOption = {this.state.ageMeasurement}
                                        onSelection = {(value) => this.ageSelectionChanged(value)}
                                    />
                                </View>
                            </View>
                        </View>

                        <Collapsible collapsed={!this.state.ageMeasurementSelected}>
                            <Animatable.View animation='fadeIn'>
                                <Slider style={{marginTop: 20}} value={this.state.age} minimumValue={1} maximumValue={this.state.maxAgeValue} step={1} onValueChange= {(value)=> this.setState({age: value})}/>
                                
                                <TouchableOpacity onPress={()=> this.setState({inputAge: !this.state.inputAge})} style={{flexDirection: 'row', alignItems:'center', justifyContent: 'center'}}>
                                    <Text style={{fontWeight: 'bold', fontSize: 20, alignItems: 'center'}}>{this.state.age}</Text>
                                    <Text style={{marginLeft: 5}}>
                                        <Text>(Maximum </Text>
                                        <Text>{this.state.maxAgeValue})</Text>
                                    </Text>
                                </TouchableOpacity>
                            </Animatable.View>
                        </Collapsible>

                        <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                            <View style={{flex:1, marginTop: 20}}>
                                <Text style={[styles.fieldTitle,]}>Gender</Text>
                            </View>
                            <View style={styles.dialog}>
                                <View style={{width: 150, justifyContent: 'flex-start'}}>
                                    <SegmentedControls
                                        //optionStyle={{borderWidth: 2}}
                                        optionContainerStyle={styles.segmentControl}
                                        tint={'#009688'}
                                        options={ genderOptions }
                                        containerBorderWidth={0}
                                        extractText={ (option) => option.label }
                                        selectedOption = {this.state.gender}
                                        onSelection = {(value) => this.genderSelectionChanged(value)}
                                    />
                                </View>
                            </View>
                        </View>
                    </Collapsible>

                    {/*Sterile Section*/}
                    <View style={{flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', marginRight: 20, marginTop: 20}}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <TouchableOpacity onPress={this.showSterilizedHelper.bind(this)}>
                                <Text>
                                    {questionCircle}
                                </Text>
                            </TouchableOpacity>
                        <Text style={[styles.fieldTitle,{marginLeft: 10}]}>Serilized</Text>
                        </View>
                       
                        <View>
                            <Switch value={this.state.sterilized} onValueChange={(value)=>this.sterilizationChanged(value)}></Switch>
                        </View>
                    </View>

                     {/*Add Button*/}
                     <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                        <TouchableHighlight style={styles.normalButton} onPress={()=> this.props.cancelClick()}>
                            <View style={{flex:1, flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                                <Text style={styles.blackText}>Cancel</Text>
                            </View>
                        </TouchableHighlight>
                        <TouchableHighlight style={visibility? styles.accentButton : styles.disabledButton} onPress={this.addClick.bind(this)}>
                            <View style={{flex:1, flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                                <Text style={styles.whiteText}>{this.state.update?'Update':'Add'}</Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        );
    }


    //Prompts
    namePrompt()
    {
       if(this.state.namePrompt)
       {
           return <Text style={{color: "red"}}>Please enter a name!</Text>
       }
    }
    breedPrompt()
    {
       if(this.state.breedPrompt)
       {
           return <Text style={{color: "red"}}>Please specify a breed!</Text>
       }
    }

    showSterilizedHelper()
    {
        Alert.alert("To breed or not to breed?", "Sterilized Pets are those that cannot be bred.",
            [{text: 'OK', onPress: () => console.log('Ask me later pressed')},])
    }
    //end of Prompts

    addClick()
    {
        var data ={
            name: this.state.name,
            breed: this.state.breed,
            age: this.state.age,
            ageMeasurement: this.state.ageMeasurement,
            gender: this.state.gender,
            sterilized: this.state.sterilized,
            avatarSource: this.props.profileURI
        }

        this.props.doneClick(data);
    }

    cancelButtonClicked()
    {
        if(this.state.update)
        {
            this.props.navigation.navigate('List', this.setState(this.initialState));
        }else{
            this.props.navigation.navigate('Addition', this.setState(this.initialState));
        }
    }

    nameEditedEnd()
    {
        var showNamePrompt = false;
        if(!this.state.name || this.state.name.length === 0)
        {
           showNamePrompt = true;
        }else{
            this.refs.breedInput.focus(); 
        }
        this.setState({name:this.capitalize(this.state.name), namePrompt: showNamePrompt});
    }

    breedEditingEnd()
    {
        var showBreedPrompt = false;
        if(!this.state.breed || this.state.breed.length === 0)
        {
           showBreedPrompt = true;
        }
        this.setState({breed : this.capitalize(this.state.breed), breedPrompt: showBreedPrompt});
    }

    nameInput(value)
    {
        if(value != '')
        {
            nameInput = true;
        }else{
            nameInput = false;
        }

        this.setState({name: value? value : ""})
        this.checkButtonVisibility();
    }

    capitalize(value)
    {   
        if(!value)
        return;

        var captalized = value.split(' ').map(function(word){
            let t = word? word[0].toString().toUpperCase():'';
            
            return t + word.substr(1);
        }).join(' ');
       
        return captalized;
    }

    breedInput(value)
    {
        if(value!= '')
        {
            breedInput = true;
        }else{
            breedInput = false;
        }
        this.setState({breed: value})
        this.checkButtonVisibility();
    }

    ageSelectionChanged(selectedValue)
    {
        ageInput = true;
        this.setState({ageMeasurement: selectedValue, ageMeasurementSelected: true}, this.setMaxAgeValue);
        this.checkButtonVisibility();
    }

    genderSelectionChanged(selectedValue)
    {
        genderInput = true;
        this.setState({gender: selectedValue});
        this.checkButtonVisibility();
    }

    sterilizationChanged(value)
    {
        this.checkButtonVisibility();
        this.setState({sterilized: value})
    }

    setMaxAgeValue(number)
    {
        if(this.state.ageMeasurement.label == "week")
        {       
            this.setState( selectedValue => {return {maxAgeValue: 3};})
        }
        if(this.state.ageMeasurement.label == "month")
        {
            this.setState( selectedValue => {return {maxAgeValue: 11};})
        }
        if(this.state.ageMeasurement.label == "year")
        {
            this.setState( selectedValue => {return {maxAgeValue: 200};})
        }
        this.setState({age: 0})
    }

    // addClick()
    // {
    //     // if(!this.state.visibility)
    //     // {
    //     //     return;
    //     // }

    //     // let props = this.props.screenProps[0].data; //pet List

    //     // //if the item is a updated item, look for it
    //     // let found = null; 
    //     // props.forEach(function(element) {
    //     //     if(element.key == this.state.key)
    //     //     {
    //     //         found = element;
    //     //         return;
    //     //     }
    //     // }, this);

    //     // if(found == null)
    //     // {
    //     //     //create new pets
    //     //     console.log("New pet created: ", this.state);
    //     //     this.props.screenProps[0].data.push({key: this.state.key, value: this.state});
    //     // }else{
    //     //     //update found pet
    //     //     found.value = this.state;
    //     //     found.value.update = false;
    //     //     console.log("Item updated: ", found);
    //     // }

    //     // let array = this.props.screenProps[0].data.slice();
    //     // this.props.screenProps[0].data = array;
    //     // this.initialState.key = guid();
    //     // this.setState(this.initialState, ()=> this.props.navigation.navigate("List", {setCreationState: this.assignState.bind(this)}));
    // }

    assignState(value)
    {
        this.setState(value);
    }

    checkButtonVisibility()
    {
        this.setState({});
    }
}

    
function guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  }

AppRegistry.registerComponent("PetCreation", ()=> PetCreation);

const styles= StyleSheet.create({
    horizontal:{
        flexDirection: 'row',
        alignItems: 'center'
    },
    profilePic:{
        width: 100,
        height: 100,
        borderRadius: 50,
    },
    fieldTitle:{
        fontSize: 15,
        marginRight: 20
    },
    textInput:{
        maxHeight: 60,
        fontSize: 30,
        fontWeight: 'bold',
        color: accentColor,
        marginRight: 20,
    },
    subTextInput:{
        maxHeight: 60,
        fontSize: 20,
        color: accentColor,
        marginRight: 20,
        color: 'black',
    },
    field:{
        marginLeft: 20,
        marginTop: 5
    },
    accentButton:{
        backgroundColor: accentColor,
        borderRadius: 20,
        width: 80,
        height: 40,
        marginRight: 20, marginTop: 30, marginBottom: 20,
        elevation: 2
    },
    disabledButton:{
        backgroundColor: 'gray',
        borderRadius: 20,
        width: 80,
        height: 40,
        marginRight: 20, marginTop: 30, marginBottom: 20,
        elevation: 2
    },
    normalButton:{
        backgroundColor: 'transparent',
        borderRadius: 20,
        width: 80,
        height: 40,
        marginRight: 20, marginTop: 30, marginBottom: 20
    },
    whiteText:{
        color:'white',
        fontWeight: 'bold'
    },
    blackText:{
        color:'black',
        fontWeight: 'bold'
    },
    segmentControl:{
        width: 20,
    },
    dialog:{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginRight: 20,
        marginTop:20
    }
})