import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    Alert
  } from 'react-native';

  const accentColor = '#009688';

  import Collapsible from 'react-native-collapsible'
  import * as Animatable from 'react-native-animatable';  

  export default class PetItemList extends Component{

    constructor(props)
    {
        super(props);
        this.state = {
            displayControls: false,
            deleteButtonPress: false
        }
    }

    render(){
        return(
        <View  style={{marginRight: 20}}>
            <TouchableHighlight style={styles.highlightContainer} underlayColor={accentColor} onPress={()=>this.itemPressed()}>
                <View  style={styles.container}>
                    <View  duration={300} style={{justifyContent: 'center'}}>
                        <Animatable.Image animation="zoomIn" style={styles.profilePic} source={this.props.data.avatarSource}></Animatable.Image>
                    </View>
                    <View style={{flexDirection: 'column', justifyContent: 'center', flex: 1, marginLeft: 10}}>
                        <Text style={styles.dogName}>{this.props.data.name}</Text>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={styles.breed}>{this.props.data.age}
                                <Text> {this.props.data.age > 0 ? this.props.data.ageMeasurement.label + "s": this.props.data.ageMeasurement.label} old |</Text>
                            </Text>
                            
                            <Text style={styles.breed}>{this.props.data.breed}</Text>
                        </View>
                        <View style={{flexDirection: 'row', marginTop: 5}}>
                            {this.props.data.gender?  <View style={styles.tag}>
                                <Text style={{color: 'white', flex: 1, marginLeft: 5, marginRight: 5}}>{this.props.data.gender.label}</Text>
                            </View> :null}
                           
                            <View style={styles.tag}>
                                <Text style={{color: 'white', flex: 1,  marginLeft: 5, marginRight: 5}}>{this.props.data.sterilized ? 'sterilized' : 'non-sterilized'}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableHighlight>

            <Collapsible collapsed={!this.state.displayControls}>
                <View style={{flexDirection: 'row', marginLeft: 20}}>
                    <TouchableHighlight style={styles.deleteButton} onPress={()=> this.displayPrompt()}>
                        <View style={{flex:1, flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                            <Text style={styles.whiteText}>Delete</Text>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.normalButton} onPress={()=>this.setState({displayControls: !this.state.displayControls}, this.props.editFunc({data: this.props.data}))}>
                        <View style={{flex:1, flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                            <Text style={styles.blackText}>Edit</Text>
                        </View>
                    </TouchableHighlight>
                </View>
            </Collapsible>
        </View>
        );
      }

      itemPressed()
      {
          console.log("Pressed");
         this.setState({displayControls: !this.state.displayControls});
      }

      displayPrompt()
      {
        Alert.alert('Delete', 'Are you sure', [
            {text: 'Yes',  onPress: ()=>  this.props.deleteFunc(this.props.data)},
            {text: 'Cancel', onPress: this.setState({displayControls: !this.state.displayControls}),style: 'cancel'}
        ],)
      }


}

  const styles = StyleSheet.create({
      highlightContainer:{
        height: 100,
        marginLeft: 20,
        marginTop: 5,
        backgroundColor:'white',
        elevation: 2
      },
      container:{
          flexDirection: 'row',
      },
      normalButton:{
        flex: 1,
        backgroundColor: 'white',
        width: 80,
        height: 40
        },
    blackText:{
        color:'black',
        fontWeight: 'bold'
    },
    whiteText:{
        color:'white',
        fontWeight: 'bold'
    },
    deleteButton:{
        flex: 1,
        backgroundColor: 'red',
        width: 80,
        height: 40
    },
      profilePic:{
        width: 70,
        height: 70,
        borderRadius: 50,
        marginLeft: 10,
        marginTop: 10,
        marginBottom: 10
    },
      dogName:{
          fontWeight: 'bold',
          fontSize: 20,
          color: accentColor
      },
      breed:{
        fontWeight: 'bold',
        fontSize: 15,
        color: 'gray',
        marginLeft: 5
    },
    tag:{
        height: 20,
        backgroundColor: 'black',
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 5
    },

  })

  AppRegistry.registerComponent('PetItemList', ()=> PetItemList);