import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  TextInput,
  Picker,
  Slider,
  Button,
  FlatList,
  ScrollView
} from 'react-native';

import * as Animatable from 'react-native-animatable';

import PetItemList from './PetItemList';
import Accordion from 'react-native-collapsible/Accordion'

const accentColor = '#009688';

export default class PetList extends Component{

    constructor(props)
    {
        super(props);
        this.state = {
            petListSource: this.props.list,
        }
        console.log("everytime");
    }

    render(){
        return(
            <View style={{flex: 1}}>
                <View style={{flexDirection:'row'}}>
                    <View>
                        <Text style={styles.header}>
                            <Text>Awesome                                
                            </Text>
                        </Text>
                        <Text style={styles.subHeader}>
                            Your Pets
                        </Text>
                    </View>
                    <View style={{flex: 1, flexDirection:'row', justifyContent:'flex-end', alignItems:'center' , marginRight: 20}}>
                        <TouchableHighlight style={styles.addButton} onPress={()=>this.props.addClick()}>
                            <Text style={styles.plusSymbol}>
                                +
                            </Text>
                        </TouchableHighlight>
                    </View>
                </View>
                <ScrollView style={{flexDirection: 'column', flex: 1}}>
                    <FlatList ref="petList"
                        style={{minHeight: 200}}
                        data={this.state.petListSource}
                        renderItem={({item}) => <PetItemList navigation={this.props.navigation}  deleteFunc={this.deleteItem.bind(this)} editFunc={this.props.editFunc} data={item.data}></PetItemList>}
                        />
                    <View style={{justifyContent:'flex-end', flexDirection:'row'}}>
                        <TouchableHighlight style={styles.normalButton} onPress={()=> this.props.cancelClick()}>
                            <View style={{flex:1, flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                                <Text style={styles.blackText}>Cancel</Text>
                            </View>
                        </TouchableHighlight>
                        <TouchableHighlight style={styles.accentButton} onPress={()=> this.props.finishClick()}>
                            <View style={{flex:1, flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                                <Text style={styles.whiteText}>Finish</Text>
                            </View>
                        </TouchableHighlight>
                     </View>
                </ScrollView>
               
            </View>
        );
    }

    getItems()
    {
        return this.props.list;
    }

    // itemPressed(item)
    // {
    //     let index = this.getIndex(item);

    //     console.log(this.refs.petList);
        
    //     let array = this.state.petListSource;

    //     array.forEach(function(element, i) {
    //         if(i!==index)
    //         {
    //             element.displayControls = false;
    //         }
    //     }, this);

    //     // array[index].displayControls = !array[index].displayControls;
    //     // console.log(array);

    //     this.state.petListSource.splice();
        
    //     // console.log(this.props.screenProps);
    //     // this.props.screenProps.splice()
    //     this.setState({petListSource: this.state.petListSource});
    // }

    deleteItem(item)
    {
        let index = this.getIndex(item);
        console.log("Index", index);
        this.props.screenProps[0].splice(index, 1);
        if(this.props.screenProps[0].length==0)
        {
            this.props.navigation.navigate("Addition");
        }
        this.setState({petListSource: this.props.screenProps[0]});
    }

    getIndex(item)
    {
        let l = this.state.petListSource;
        let index = l.findIndex(i => i.key == item.key);
        console.log("Index Found", index);
        return index;
    }

    editItem(item)
    {
        item.data.update = true;
        item.object.state.displayControls = false;
        console.log("State", item.object);
        this.props.navigation.state.params.setCreationState(item.data)
        this.props.navigation.navigate("Creations");
    }

    finishedButtonClicked()
    {
    }
}

const styles = StyleSheet.create(
    {
        header:{
            fontSize: 30,
            marginLeft: 20,
            color: 'black'
        },
        headerBold:{
            fontWeight: 'bold'
        },
        subHeader:{
            fontSize: 20,
            marginLeft:20
        },
        accentButton:{
            backgroundColor: accentColor,
            borderRadius: 20,
            width: 80,
            height: 40,
            marginRight: 20, marginTop: 30, marginBottom: 20,
            elevation: 2
        },
        normalButton:{
            backgroundColor: 'transparent',
            borderRadius: 20,
            width: 80,
            height: 40,
            marginRight: 20, marginTop: 30, marginBottom: 20
        },
        whiteText:{
            color:'white',
            fontWeight: 'bold'
        },
        blackText:{
            color:'black',
            fontWeight: 'bold'
        },
        addButton:{
            width: 50,
            height: 50,
            backgroundColor: '#009688',
            borderRadius: 50,
            marginRight: 10
        },
        plusSymbol:{
            flex:1,
            fontSize: 25,
            color: 'white',
            textAlign: 'center',
            textAlignVertical: 'center',
        },
    }
)

AppRegistry.registerComponent("PetList", ()=> PetList);