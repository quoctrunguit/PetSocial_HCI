import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableHighlight
  } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

const myButton = (
    <Icon.Button name="facebook" backgroundColor='transparent' color='#2699FB' onPress={this.loginWithFacebook}> Login with facebook</Icon.Button>
    );


export default class SignIn extends Component{

    render(){
        return(
            // <View style={{flex: 1}}>
            //         <View >
            //         <TextInput style={styles.textInput}
            //                     placeholder='username'></TextInput>
            //     </View>
            //     <View>
            //         <TextInput style={styles.textInput}
            //                     placeholder='password'></TextInput>
            //     </View>
            //     <View style={styles.loginContainer}>
            //         <View  style={styles.rowContainer}>
            //             <View>
            //                 <Text style={styles.extLink}>Forgot my password?</Text>
            //             </View>
            //             <View style={{marginLeft: 20}}>
            //                 <TouchableHighlight style={styles.button}  onPress={()=> this.props.navigation.navigate('WelcomePage')}>
            //                     <View style={{flex:1, flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
            //                         <Text style={styles.whiteText}>LOG IN</Text>
            //                     </View>
            //                 </TouchableHighlight>
            //             </View>
            //         </View>
            //     </View>
            //     <View style={{height: 150}}>
            //         <View style={styles.rowContainerCentered}>
            //             {myButton}
            //         </View>
            //     </View> 
            // </View>
            <View style = {{flex: 1}}>
                <View style ={styles.container}>
                    <View>
                        
                    </View>
                </View>

            </View>
        );
    }
}


    const styles = StyleSheet.create(
        {
            container:{
                flex: 1,
                borderWidth: 1,           
            },
            textInput:{
                width: 250,
                height: 50
            },
            loginContainer:{
                width: 'auto',
                height:50,
                marginTop: 20
            },
            borderContainer:{
                borderWidth: 2
            },
            extLink:{
                flex:1,
                fontSize:10,
                marginTop: 20,
                alignContent: 'flex-end'
            },
            rowContainer:
            {
                flex:1,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent:'flex-end'
            },
            rowContainerCentered:
            {
                flex:1,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent:'center'
            },
            topContainter:
            {
                flexDirection:'column',
                alignContent:'flex-start',
                borderWidth: 2
            },
            button:{
                backgroundColor: '#009688',
                borderRadius: 20,
                width: 80,
                height: 40
            },
            border:{
                borderWidth: 2
            },
            whiteText:{
                color:'white',
                fontWeight: 'bold'
            }
        }
    )
    

AppRegistry.registerComponent("SignIn", ()=> SignIn);