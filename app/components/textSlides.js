import React, {Component } from 'react';

import {
    Platform,
    StyleSheet,
    Text,
    View,
} from 'react-native';

import * as Animatable from "react-native-animatable"

const logoSlides = ["🐶", "😽", "🐵", "🐷","🦅","🐇"]
const adjectives = ["fluffy", "furry", "unique", "thingy", "bitey", "clawey"]
const accentColor = '#009688';
const slides = 0;

export default class TextSlides extends React.PureComponent<{}>{
    constructor(props){
        super(props);
        this.state = {
            logo: "🐶",
            adj: "fluffy",
        }
    }

    componentDidMount(props)
    {
        var count = 0;
        slides = setInterval(()=>{
            if(count == logoSlides.length)
                count =0 ;
            
            this.setState({logo: logoSlides[count++], adj: adjectives[count]});
        }, 1000);
    }

    componentWillUnmount()
    {
        clearInterval(slides)
    }

    render()
    {
        return(
            <View style={{flex: 1, justifyContent: 'center', opacity: 0.7}}>
                <Text style={{fontSize: 31, marginLeft: 20, lineHeight: 50}}>
                    Join {"\n"}
                    the{"\n"}
                    <Text style={{color: accentColor}}>{this.state.adj} {this.state.logo}{"\n"}</Text>
                    social 
                </Text>
            </View>
        )
    }
}


