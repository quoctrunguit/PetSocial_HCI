import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  TextInput,
  Picker,
  Slider,
  Button,
  TouchableWithoutFeedback,
  Switch
} from 'react-native';

import Collapsible from 'react-native-collapsible';
import Icon from 'react-native-vector-icons/FontAwesome';
import {TabNavigator, StackNavigator} from 'react-navigation';

import PetCreation from './petCreation';
import PetList from './petList';
import PetAddition from './petAddition';

const accentColor = '#009688';

const petList = [];


export default class SetupStack extends Component{

    constructor(props)
    {
        super(props)
    }

    render(){
        return(
            <Navigator ref="navigator" screenProps={[{key: 'list', data: petList},{key: 'nav', data:  this.props.navigation}]} onNavigationStateChange = {(prevState, newState, action)=>this.navigating(prevState, newState, action)}/>
        );
    }
    navigating(prevState, newState, action)
    {
        console.log(this.refs.navigator)
    }
}



const Navigator = TabNavigator({
    Addition: {
        screen: PetAddition,
        navigationOptions : {
          tabBarLabel: 'PetCreation',
          tabBarVisible: false,
          animationEnabled: true,
          header:{
              visible: false
          },
          },
      },
    Creations: {
      screen: PetCreation,
      navigationOptions : {
        tabBarLabel: 'PetCreation',
        tabBarVisible: false,
        animationEnabled: true,
        header:{
            visible: false
        },
        },
    },
    List: {
      screen: PetList,
      navigationOptions : {
        tabBarLabel: 'PetList',
        tabBarVisible: false,
        animationEnabled: true,
        header:{
            visible: false
        },
      },
  }
},
{
    swipeEnabled: false,
},
);

const styles= StyleSheet.create({
    horizontal:{
        flexDirection: 'row',
        alignItems: 'center'
    },
    profilePic:{
        width: 100,
        height: 100,
        borderRadius: 50,
    },
    fieldTitle:{
        fontSize: 15,
        marginRight: 20
    },
    textInput:{
        maxHeight: 60,
        fontSize: 30,
        fontWeight: 'bold',
        color: accentColor,
        marginRight: 20
    },
    field:{
        marginLeft: 20,
        marginTop: 5
    },
    accentButton:{
        backgroundColor: accentColor,
        borderRadius: 20,
        width: 80,
        height: 40,
        marginRight: 20, marginTop: 30, marginBottom: 20
    },
    normalButton:{
        backgroundColor: 'transparent',
        borderRadius: 20,
        width: 80,
        height: 40,
        marginRight: 20, marginTop: 30, marginBottom: 20
    },
    whiteText:{
        color:'white',
        fontWeight: 'bold'
    },
    blackText:{
        color:'black',
        fontWeight: 'bold'
    },
    segmentControl:{
        width: 20,
    },
    dialog:{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginRight: 20,
        marginTop:20
    }
})

AppRegistry.registerComponent('SetupStack', ()=> SetupStack);