import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  TextInput,
  Picker,
  Slider,
  Button,
  TouchableWithoutFeedback,
  Switch,
  Dimensions
} from 'react-native';

import Collapsible from 'react-native-collapsible';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as Animatable from 'react-native-animatable'; 

var radio_props = [
    {label: 'param1', value: 0 },
    {label: 'param2', value: 1 }
];

const accentColor = '#009688';
const {width, height} = Dimensions.get("window");
const buttonSize = width*0.3

export default class PetAddition extends React.PureComponent{

    constructor(props)
    {
        super(props);
    }

    componentDidMount()
    {

    }

    render(){

        var wireframe= this.props.showGridlines ? styles.wireframe : null;

        return(
            <Animatable.View style={[{flex: 1}]}>
                <Animatable.View animation='slideInUp' style={[{flex: 0.3}, wireframe]}>
                    <Animatable.Text   style={styles.header}>
                        <Text>Welcome to </Text> 
                            <Text style={styles.headerBold}>
                            Woofie!                                 
                        </Text>
                    </Animatable.Text>
                    <Text style={styles.subHeader}>
                        Let's add your pets!
                    </Text>
                </Animatable.View>
                
                <View  style={[{flex: 0.7, alignItems:'center'}, wireframe]}>
                    <Animatable.View  animation="zoomIn" duration={1000}  easing="ease-out"  style={styles.addButton}>
                        <TouchableHighlight style={styles.highlight} underlayColor={accentColor} onPress={()=> this.props.addClick()}>
                            <Text style={styles.plusSymbol}  duration={300} >
                                +
                            </Text>
                        </TouchableHighlight>
                    </Animatable.View>
                </View>
            </Animatable.View>
        );
    }
}

const styles = StyleSheet.create(
    {
        dogPaw:{
            height:180,
            width: 210,
            transform: [{rotateZ:'180deg'}]
        },
        welcome:{
            flex:1,
            flexDirection: 'column',
            alignItems: 'center',
            marginTop: 150
        },
        header:{
            fontSize: 30,
            marginLeft: 20,
            color: 'black'
        },
        headerBold:{
            fontWeight: 'bold'
        },
        subHeader:{
            fontSize: 20,
            marginLeft:20
        },
        plusSymbol:{
            flex:1,
            fontSize: 50,
            color: 'white',
            textAlign: 'center',
            textAlignVertical: 'center',
        },
        addButton:{
            width: buttonSize,
            height: buttonSize,
            marginTop: 0,
            elevation: 10,
            backgroundColor: 'black',
            backgroundColor: '#009688',
            borderRadius: buttonSize/2,
            alignItems: 'center',
        },
        highlight:{
            width: 100,
            height: 100,
            borderRadius: 50,
        },
        wireframe: {
            borderWidth: 1,
        }
    }
)

AppRegistry.registerComponent("PetAddition", ()=> PetAddition);