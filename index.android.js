/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  StatusBar,
  FlatList,
  Image,
  Navigator,
  Button,
  Easing,
  Animated
} from 'react-native';

import { StackNavigator } from 'react-navigation';


// import HomePage from './app/views/homepage';
// import Welcome from './app/views/welcome';
// import Timeline from './app/views/timeline';

// const PetSocial = StackNavigator({
//   Home: {
//     screen: HomePage,
//     navigationOptions: {
//         headerTitle: 'Home',
//       },
//   },
//   WelcomePage: {
//     screen: Welcome,
//     navigationOptions: {
//         headerTitle: 'Welcome',
//         },
//     },
//   Timeline: {
//     screen: Timeline,
//     navigationOptions: {
//         headerTitle: 'Timeline',
//         },
//     },
// },
// {
//   headerMode: 'none',
//   mode: 'modal',
//   navigationOptions: {
//     gesturesEnabled: false,
//   },
// }
// );

import {Root, Tabs} from './app/config/router'

const petList = [];

export default class PetSocial extends Component<{}>{
  render()
  {
    return(
      <Root screenProps={{list: petList}}/>
    )
  }
}

AppRegistry.registerComponent('PetSocial',() => PetSocial);